package dat.lt.fes.common.validate;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface NotBlankValidator {
    String message() default "{javax.validation.constraints.NotNull.message}";
    String[] messageParam() default {};
}
