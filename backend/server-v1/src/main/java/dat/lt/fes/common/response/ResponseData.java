package dat.lt.fes.common.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * response class with a class as data and information of response extends from ResponseNoData
 * @param <T> type of data that implements Serializable
 */
@Getter
@Setter
@NoArgsConstructor
public class ResponseData<T extends Serializable> extends ResponseNoData {
    private T data; //
}
