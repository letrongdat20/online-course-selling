package dat.lt.fes.common.datetime;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * class DateTimeUtils contains static method that converts date format.
 * Primary element of this class is ISO format and time.* package.
 * Other format will convert to ISO date format ("yyyy-MM-dd HH:mm:ss") and classes in time package
 */
public class DateTimeUtils {
    public static final String ISO_DATE = "yyyy-MM-dd";
    public static final String ISO_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
    public static final String ISO_TIME = "HH:mm:ss";
    public static final String UTC_DATE_TIME = "yyyy-MM-ddTHH:mm:ssZ";

    /**
     * @param datetime - time.LocalDateTime
     * @param format   - String - format of time want to convert
     * @return - String - time with @param format
     */
    public static String convertToStr(LocalDateTime datetime, String format) {
        try {
            return datetime.format(DateTimeFormatter.ofPattern(format));
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * @param date - time.LocalDate
     * @return - String - date with format @param format
     */
    public static String convertToStr(LocalDate date, String format) {
        try {
            return date.format(DateTimeFormatter.ofPattern(format));
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * @param date - time.LocalTime
     * @return - String - time with format @param format
     */
    public static String convertToStr(LocalTime date, String format) {
        try {
            return date.format(DateTimeFormatter.ofPattern(format));
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * convert time.LocalDateTime to sql.Date if cannot convert return null
     *
     * @param datetime - time.LocalDateTime
     * @return - sql.Date
     */
    public static java.sql.Date convertToSqlDate(LocalDateTime datetime) {
        try {
            return java.sql.Date.valueOf(datetime.toLocalDate());
        } catch (NullPointerException e) {
            return null;
        }
    }

    /**
     * convert time.LocalDate to sql.Date if cannot convert return null
     *
     * @param date - time.LocalDate
     * @return - sql.Date
     */
    public static java.sql.Date convertToSqlDate(LocalDate date) {
        try {
            return java.sql.Date.valueOf(date);
        } catch (NullPointerException e) {
            return null;
        }
    }

    /**
     * convert LocalDateTime to TimeStamp if cannot return null
     *
     * @param datetime - LocalDateTime
     * @return - sql.TimeStamp
     */
    public static Timestamp convertToTimestamp(LocalDateTime datetime) {
        try {
            return Timestamp.valueOf(datetime);
        } catch (NullPointerException e) {
            return null;
        }
    }

    /**
     * convert LocalDate to Timestamp return null if cannot convert
     *
     * @param date - LocalDate
     * @return - sql.Timestamp
     */
    public static Timestamp convertToTimestamp(LocalDate date) {
        try {
            return Timestamp.valueOf(date.atStartOfDay());
        } catch (NullPointerException e) {
            return null;
        }
    }

    /**
     * convert LocalDate to util.Date return null if cannot convert
     *
     * @param date - LocalDate
     * @return - util.Date
     */
    public static java.util.Date convertToDate(LocalDate date) {
        try {
            return java.sql.Date.valueOf(date);
        } catch(NullPointerException e) {
            return null;
        }
    }

    /**
     * convert LocalDateTime to util.Date return null if cannot convert
     *
     * @param date - LocalDateTime
     * @return - util.Date
     */
    public static java.util.Date convertToDate(LocalDateTime date) {
        try {
            return java.sql.Timestamp.valueOf(date);
        } catch(NullPointerException e) {
            return null;
        }
    }

    /**
     * convert TimeStamp to LocalDateTime return null if cannot convert
     *
     * @param timestamp - java.sql.TimeStamp
     * @return - LocalDateTime
     */
    public static LocalDateTime convertToLocalDateTime(Timestamp timestamp) {
        try {
            return timestamp.toLocalDateTime();
        } catch(NullPointerException e) {
            return null;
        }
    }

    /**
     * convert util.Date to LocalDateTime return null if cannot convert
     *
     * @param date - java.util.Date
     * @return - LocalDateTime
     */
    public static LocalDateTime convertToLocalDateTime(java.util.Date date) {
        try {
            return new java.sql.Timestamp(date.getTime()).toLocalDateTime();
        } catch(NullPointerException e) {
            return null;
        }
    }

    /**
     * convert sql.Date to LocalDateTime return null if cannot convert
     *
     * @param date - java.sql.Date
     * @return - LocalDateTime
     */
    public static LocalDateTime convertToLocalDateTime(java.sql.Date date) {
        try {
            return date.toLocalDate().atStartOfDay();
        } catch(NullPointerException e) {
            return null;
        }
    }

    /**
     * convert sql.Date to LocalDat return null if cannot convert
     *
     * @param date - sql.Date
     * @return - LocalDate
     */
    public static LocalDate convertToLocalDate(java.sql.Date date) {
        try {
            return date.toLocalDate();
        } catch (NullPointerException e) {
            return null;
        }
    }

    /**
     * convert date string to LocalDateTime
     *
     * @param str    - date representation string
     * @param format - format of @param str
     * @return - LocalDateTime
     */
    public static LocalDateTime strToLocalDateTime(String str, String format) {
        try {
            return LocalDateTime.parse(str, DateTimeFormatter.ofPattern(format));
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * convert date string to LocalDateTime
     *
     * @param str    - date representation string
     * @return - LocalDateTime
     */
    public static LocalDateTime toLocalDateTime(String str) {
        try {
            return LocalDateTime.parse(str);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * convert date string to LocalDate
     *
     * @param str    - date representation string
     * @param format - format of @param str
     * @return - LocalDate
     */
    public static LocalDate strToLocalDate(String str, String format) {
        try {
            return LocalDate.parse(str, DateTimeFormatter.ofPattern(format));
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * convert date string to LocalDate
     *
     * @param str    - date representation string
     * @return - LocalDate
     */
    public static LocalDate toLocalDate(String str) {
        try {
            return LocalDate.parse(str);
        } catch (Exception e) {
            return null;
        }
    }
}
