package dat.lt.fes.entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;


@Data
@Entity
@Table(name = "SCHOOL_PROFILE")
@IdClass(SchoolProfileKey.class)
public class SchoolProfile implements Serializable {
    @Column(name = "STUDENT_ID")
    @Id
    private Long studentId;
    @Column(name = "COURSE_ID")
    @Id
    private Long courseId;

}

@Setter
@Getter
@NoArgsConstructor
class SchoolProfileKey implements Serializable{
    private Long courseId;
    private Long studentId;

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof SchoolProfileKey) {
            SchoolProfileKey key = (SchoolProfileKey) obj;
            if (this.studentId == null) {
                if (key.getStudentId() != null)
                    return false;
            } else {
                if (!this.studentId.equals(key.getStudentId())) {
                    return false;
                }
            }
            if (this.courseId == null) {
                if (key.getCourseId() != null)
                    return false;
            } else {
                if (!this.courseId.equals(key.getCourseId())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}