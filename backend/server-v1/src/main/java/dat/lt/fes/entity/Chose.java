package dat.lt.fes.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "CHOSE")
@Setter
@Getter
@NoArgsConstructor
@IdClass(ChoseKey.class)
public class Chose implements Serializable {
    @Id
    @Column(name = "STUDENT_ID")
    private Long studentId;
    @Id
    @Column(name = "CHOICE_ID")
    private Long choiceId;
    @Id
    @Column(name = "TIME")
    private Integer time;
}

@NoArgsConstructor
@Setter
@Getter
class ChoseKey implements Serializable {
    Long studentId;
    Long choiceId;
    private Integer time;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ChoseKey) {
            ChoseKey choseKey = (ChoseKey) obj;
            if (this.studentId == null) {
                if (choseKey.getStudentId() != null)
                    return false;
            } else {
                if (!this.studentId.equals(choseKey.getStudentId())) {
                    return false;
                }
            }
            if (this.choiceId == null) {
                if (choseKey.getChoiceId() != null)
                    return false;
            } else {
                if (!this.choiceId.equals(choseKey.getChoiceId())) {
                    return false;
                }
            }
            if (this.time == null) {
                if (choseKey.getTime() != null) {
                    return false;
                }
            } else {
                if (!this.time.equals(choseKey.getTime())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}