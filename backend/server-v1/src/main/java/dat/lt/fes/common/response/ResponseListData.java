package dat.lt.fes.common.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Collection;

/**
 * response class with a list as data and information of response extends from ResponseNoData
 * @param <T> type of data that implements Serializable
 */
@NoArgsConstructor
@Setter
@Getter
public class ResponseListData<T extends Serializable> extends ResponseNoData{
    Collection<T> data;
}
