package dat.lt.fes.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name = "A5_STUDENT")
@NoArgsConstructor
@Setter
@Getter
public class Student implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "STUDENT_ID")
    private Long id;

    @Column(name = "FIRST_NAME")
    private String firstName;
    @Column(name = "LAST_NAME")
    private String lastName;
    @Column(name = "BIRTH_DAY")
    private Date birthDay;
    @Column(name = "AVATAR")
    private String avatar;
    @Column(name = "JOB")
    private String job;
    @Column(name = "PHONE")
    private String phone;
    @Column(name = "LAST_LOGIN")
    private Date lastLogin;
    @Column(name = "OFFICE")
    private String office;

    @JoinTable(name = "STUDENT_HOBBY")
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Hobby> hobbies;
}
