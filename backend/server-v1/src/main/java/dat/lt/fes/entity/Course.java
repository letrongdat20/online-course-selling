package dat.lt.fes.entity;

import dat.lt.fes.common.validate.NotNullValidator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@NoArgsConstructor
@Setter
@Getter
@Table(name = "C1_COURSE")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "COURSE_ID")
    private long id;

    @Column(name = "TITLE")
    @NotNull
    @Max(500)
    private String title;
    @Column(name = "DESCRIPTION")
    @Max(500)
    private String description;
    @Column(name = "RATE")
    @Max(5)
    @Min(0)
    @NotNull
    private int rate;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "courseId")
    private List<SchoolProfile> profiles;
}
