package dat.lt.fes.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "T3_CHOICE")
@Setter
@Getter
@NoArgsConstructor
public class Choice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CHOICE_ID")
    private Long id;

    @Column(name = "CHOICE_TEXT")
    @NotNull
    private String choiceText;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "QUESTION_ID", nullable = false)
    private Question question;
}
