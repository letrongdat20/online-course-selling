package dat.lt.fes.common.validate;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface StringLength {
    int min() default 0;
    int max() default Integer.MAX_VALUE;
    String message() default "{javax.validation.constraints.NotNull.message}";
    String [] messageParam() default {};
}
