package dat.lt.fes.common.validate;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * spring bean that config custom annotation
 */
@Component
@Aspect
public class AnnotationAspect {

    /**
     * Config for StringLength annotation
     */
    @Around(value = "@annotation(strLength)")
    public Object stringLength(ProceedingJoinPoint joinPoint, StringLength strLength) throws Throwable {
        Object[] args = joinPoint.getArgs();
        Object result = null;
        if (args != null && args.length > 0) {
            String min = (String) args[0];
            String max = (String) args[1];
            String message = (String) args[2];
            String messageParam = (String) args[3];
        }
        try {
            result = joinPoint.proceed();
        } catch(Throwable e) {
            e.printStackTrace();
        }
        return result;
    }
}
