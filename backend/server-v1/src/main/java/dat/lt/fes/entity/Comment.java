package dat.lt.fes.entity;

import dat.lt.fes.common.validate.NotNullValidator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "COMMENT")
@NoArgsConstructor
@Setter
@Getter
public class Comment implements Serializable {
    @Id
    @Column(name = "COMMENT_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "COURSE_ID")
    @NotNull
    private Course course;
    @ManyToOne
    @JoinColumn(name = "STUDENT_ID")
    @NotNull
    private Student student;

    @Column(name = "COMMENT")
    @NotNull
    private String comment;
}
