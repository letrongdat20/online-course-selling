package dat.lt.fes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerV1Application {

    public static void main(String[] args) {
        SpringApplication.run(ServerV1Application.class, args);
    }

}
