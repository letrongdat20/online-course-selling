package dat.lt.fes.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Information, history of admin
 */
@Table(name = "A2_ADMIN")
@Entity
@NoArgsConstructor
@Setter
@Getter
public class Admin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ADMIN_id")
    private long courseId;

    @Column(name = "OFFICE")
    private String office;
}
