package dat.lt.fes.common.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * response class contains status, message, and message code as information of response
 */
@Setter
@Getter
@NoArgsConstructor
public class ResponseNoData implements Serializable {
    private String status; // status of response
    private String message; // message of response
    private String code; // message code that use to find message from file
}
