package dat.lt.fes.entity;

import javax.persistence.*;

@Entity
@Table(name = "T1_QUESTION")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "QUESTION_ID")
    private Long questionId;
}
