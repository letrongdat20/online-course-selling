package dat.lt.fes.entity;

import dat.lt.fes.common.validate.NotBlankValidator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;

/**
 * Entity that save information and status of account
 */
@Entity
@NoArgsConstructor
@Setter
@Getter
@Table(name = "A1_ACCOUNT")
public class Account implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ACCOUNT_ID")
    private Long id;

    @Column(name = "USERNAME")
    @NotBlank
    private String userName;

    // Account registration email
    @Column(name = "EMAIL")
    @Email
    private String email;

    @Column(name = "PASSWORD")
    @NotBlank
    private String password;

    // Date that create account
    @Column(name = "CREATE_DATE")
    private Date createDate;

    // Status of account from 0 to 5
    // 0: registration, not active email
    // 1: activated email
    // 2: active member
    // 3: vip
    // 4: blocked
    @Column(name = "STATUS")
    @Min(0)
    @Max(5)
    private int status;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "ACCOUNT_ROLE",
            joinColumns = @JoinColumn(name = "ACCOUNT_ID"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID"))
    private List<Role> roles;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "STUDENT_ID")
    private Student information;
}
