import { COURSE_1 } from 'guess/const';

export const getCourse = (id) => {
    return {
        type: "course",
        payload: COURSE_1
    }
}
export const enroll = (id) => {
    return {
        type: "enroll",
        payload: COURSE_1
    }
}