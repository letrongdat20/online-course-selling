import React from 'react';
import { connect } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import Topbar from 'base_component/Topbar';
import Footer from 'base_component/Footer';
import * as action from 'guess/course-view/CourseAction';
const mapStateToProps = (state) => {
    return {
        course: state.course
    }
}
const mapDispatchtoProps = (dispatch) => {
    return {
        enroll: (id) => dispatch(action.enroll(id))
    }
}
function CourseView(props) {
    const history = useHistory();
    const { course } = useParams();
    let isLoggedIn = "true";
    const enroll = (id) => {
        if (isLoggedIn === "false") {
            isLoggedIn = "true";
            history.push("/login");
        }
        else {
            props.enroll(id);
            history.push(`/courses/${id}/class`);
        }
    }
    return (
        <div>
            <Topbar isLoggedIn="true" />
            <div className="bg-blue p-4">
                <div className="parallelogram d-none d-lg-block" style={{ right: "250px", top: "0px" }}></div>
                <div className="container p-0">
                    <div className="row">
                        <div className="col-10 col-lg-6 py-3">
                            <h1 className="text-60 text-white mr-3">{props.course.title}</h1>
                            <div className="text-20 text-white">{props.course.subTitle}</div>
                        </div>
                        <div className="col-8 col-lg-5">
                            <div className="text-white text-20 pb-2">Own by</div>
                            <h2 className="pl-4 text-white text-36">{props.course.author}</h2>
                        </div>
                    </div>
                    <div className="my-3">
                        <button className="btn btn-danger btn-lg" onClick={() => enroll(course)}>Enroll Now</button>
                    </div>
                </div>
            </div>
            <div className="container py-5">
                <div className="d-flex justify-content-center">
                    <div className="col-10 col-md-8">
                        <div>
                            <h1 className="pb-3">About this course</h1>
                            <p className="text-25-thin">{props.course.decription}</p>
                        </div>
                        <div>
                            <h1 className="py-3">What you'll learn</h1>
                            <ul>
                                {props.course.learn.map((item, index) => {
                                    return <li key={index}>
                                        <p className="text-20">{item}</p>
                                    </li>
                                })}
                            </ul>
                        </div>
                        <div>
                            <h1 className="py-3">Lesson</h1>
                            <ul className="list-unstyled">
                                {props.course.lesson.map((item, index) => {
                                    return <li key={index}>
                                        <p className="text-20">Week {index + 1}: {item}</p>
                                    </li>
                                })}
                            </ul>
                        </div>
                        <div>
                            <h3 className="pb-3"><b>At a glance</b></h3>
                            <ul>
                                <li><p className="text-20"><b>Institution:</b> {props.course.school}</p></li>
                                <li><p className="text-20"><b>Subject:</b> {props.course.subject}</p></li>
                                <li><p className="text-20"><b>Level:</b> {props.course.level}</p></li>
                                <li><p className="text-20"><b>Language:</b> {props.course.language}</p></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}
export default connect(mapStateToProps, mapDispatchtoProps)(CourseView)