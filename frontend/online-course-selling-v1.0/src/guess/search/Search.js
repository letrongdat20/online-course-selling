import React, { useState } from 'react';
import { connect } from 'react-redux';
import Topbar from 'base_component/Topbar';
import Icon from "base_component/Icon";
import Footer from 'base_component/Footer';
import * as action from 'guess/search/SearchAction';
const mapStateToProps = (state) => {
    return {
        ...state.search
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        search: (infor) => dispatch(action.search(infor))
    }
}
/**
 * @returns component that render search page
 */
const searchTab = ["all", "courses", "project"]
function Search(props) {
    const [tab, setTab] = useState(0);
    const search = (tab) => {
        setTab(tab);
        var infor = {
            type: searchTab[tab],
            subject: "",
            level: "",
            school: "",
            language: "",
            available: ""
        }
        props.search(infor);
    };
    return (
        <div>
            <Topbar isLoggedIn={true} />
            <div className="container-fluit pt-3 pt-lg-5 pb-0 bg-blue">
                <form className="container">
                    <h4 className="mb-2 text-white font-weight-bold">Search Course</h4>
                    <div className="form-row">
                        <div className="col-9"><input className="form-control form-control-lg" type="text" placeholder="Tìm khóa học" /></div>
                        <div><button className="btn btn-block btn-lg btn-danger ml-1" type="submit">Tìm kiếm</button></div>
                    </div>
                    <ul className="list-unstyled d-flex flex-wrap">
                        <li className="mt-3 mr-3">
                            <select className="btn btn-block bg-gray" name="subject">
                                <option value="">Subject</option>
                                <option value="science">Science</option>
                                <option value="art">Art & Culture</option>
                                <option value="business">Business</option>
                            </select>
                        </li>
                        <li className="mt-3 mr-3">
                            <select className="btn btn-block bg-gray" name="level">
                                <option value="">Level</option>
                                <option value="beginner">Introduction</option>
                                <option value="medium">Medium</option>
                                <option value="advanced">Advanced</option>
                            </select>
                        </li>
                        <li className="mt-3 mr-3">
                            <select className="btn btn-block bg-gray" name="school">
                                <option value="">School</option>
                                <option value="hust">HUST</option>
                                <option value="neu">NEU</option>
                                <option value="nuce">NUCE</option>
                            </select>
                        </li>
                        <li className="mt-3 mr-3">
                            <select className="btn btn-block bg-gray" name="language">
                                <option value="">Language</option>
                                <option value="python">Python</option>
                                <option value="java">Java</option>
                                <option value="C++">C++</option>
                            </select>
                        </li>
                        <li className="mt-3 mr-3">
                            <select className="btn btn-block bg-gray" name="">
                                <option value="">Available</option>
                                <option value="now">Now</option>
                                <option value="upcoming">Upcoming</option>
                                <option value="archived">Archived</option>
                            </select>
                        </li>
                    </ul>
                    <div className="d-flex flex-row mt-3 mt-lg-4" id="search-tab">
                        <div role="button" className="btn-toolbar tab-hover bg-white py-2 px-5 mr-2" style={{ opacity: 0.7 }}
                            aria-expanded={tab === 0 ? "true" : "false"} onClick={() => { search(0) }}>All</div>
                        <div role="button" className="btn-toolbar tab-hover bg-white py-2 px-4 mr-2" style={{ opacity: 0.7 }}
                            aria-expanded={tab === 1 ? "true" : "false"} onClick={() => { search(1) }}>Course</div>
                        <div role="button" className="btn-toolbar tab-hover bg-white py-2 px-1" style={{ opacity: 0.7 }}
                            aria-expanded={tab === 2 ? "true" : "false"} onClick={() => { search(2) }}>Guided Project</div>
                    </div>
                </form>
            </div>
            <div className="container-fluit py-3 py-lg-5">
                <div className="container px-0">
                    <div>
                        <h2 className="font-weight-bold my-3 ml-3">Courses ({props.volumn} results)</h2>
                        <div className="d-flex flex-wrap">
                            {props.courses.map((item, index) => {
                                return (
                                    <Icon key={index} {...item} className="col-6 col-md-4 col-lg-3 my-3" />
                                );
                            })}
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(Search);