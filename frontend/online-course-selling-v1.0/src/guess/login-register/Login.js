import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import * as actions from "guess/login-register/LoginAction";

/**
 * redux function that send state to dispatcher
 * this function will return props of component -> we can use that function
 * in return value as a property of props
 * @param {function} dispatch with @param{action} that will call action 
 * @return {props} of component
 */
const mapDispatchToProps = dispatch => ({
    setUser: (user) => {
        dispatch(actions.setUser(user))
    }
});

/**
 * login component
 */
class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: "",
            password: "",
            userMessage: "",
            passwordMessage: ""
        }
    }

    userChange = (event) => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({ [name]: value });
        if (!value) {
            this.setState({ userMessage: "Requied" });
        }
        else {
            this.setState({ userMessage: "" });
        }
    }

    passwordChange = (event) => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({ [name]: value });
        if (!value) {
            this.setState({ passwordMessage: "Requied" });
        }
        else {
            this.setState({ passwordMessage: "" });
        }
    }

    submit = (event) => {
        event.preventDefault();
        var error = 0
        if (!this.state.password) {
            this.setState({ passwordMessage: "Requied" })
            error += 1
        }
        if (!this.state.userName) {
            this.setState({ userMessage: "Requied" })
            error += 1
        }
        if (error === 0) {
            const user = {
                username: this.state.userName,
                password: this.state.password,
                avatar: "public/image/avatar/avatar.png"
            };
            axios({
                method: "POST",
                url: `/striker/account/login`,
                data: user
            })
                .then(res => {
                    if (res.data.token) {
                        localStorage.setItem("token", res.data.token);

                        // login success, redirect to last page
                        this.props.setUser(res.data);
                        if (this.props.history.length > 2)
                            this.props.history.goBack();
                        else this.props.history.push("/");
                    }
                    else {
                        this.setState({ passwordMessage: "Invalid user or password" });
                    }
                })
                .catch(error => {
                    // alert("Can not connect to server");
                    this.props.setUser(user);
                    if (this.props.history.length > 2)
                        this.props.history.goBack();
                    else this.props.history.push("/");
                });
        }
    }

    render() {
        // login page
        return (
            <div className="container-fluid bg-gradient-primary py-5">
                <div className="container">
                    <div className="d-flex">
                        <div className="col-lg-10 col-xl-9 mx-auto">
                            <div className="card card-signin flex-row align-items-center">
                                <div className="card-img-left d-none d-md-flex col-md-5 col-lg-6"
                                    style={{ backgroundImage: `url('/image/background/bg-register.jpg')` }}>
                                    {/* Background image for card set in CSS! */}
                                </div>
                                <div className="card-body col-md-7 col-lg-6">
                                    <h5 className="card-title text-center">Welcome back!</h5>
                                    <form className="form-signin" onSubmit={this.submit}>
                                        {/* user name input */}
                                        <div className="form-label-group">
                                            <input type="text"
                                                id="userName"
                                                className="form-control"
                                                placeholder="User Name"
                                                autoFocus
                                                name="userName"
                                                onBlur={this.userChange} />
                                            <span className="overlay"></span>
                                            {/* error */}
                                            <span className="error">{this.state.userMessage}</span>
                                        </div>
                                        {/* password input */}
                                        <div className="form-label-group">
                                            <input type="password"
                                                id="password"
                                                className="form-control"
                                                placeholder="Password"
                                                name="password"
                                                onBlur={this.passwordChange} />
                                            <span className="overlay"></span>
                                            {/* error */}
                                            <span className="error">{this.state.passwordMessage}</span>
                                        </div>
                                        {/* remember checkbox */}
                                        <div className="custom-control custom-checkbox mb-3">
                                            <input type="checkbox"
                                                className="custom-control-input"
                                                name="remember" id="remember" />
                                            <label htmlFor="remember"
                                                className="custom-control-label">Remember Password
                                            </label>
                                        </div>
                                        {/* submit button */}
                                        <button className="btn btn-lg btn-primary btn-block text-uppercase"
                                            type="submit">Log In</button>
                                        <Link to="/register"
                                            className="btn btn-lg btn-success btn-block text-uppercase"
                                            href="#register">Register</Link>
                                        <div className="text-center m-3">
                                            <Link className="small"
                                                to="/forgot-password">Forgot password?</Link>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

//connect component to redux and export
export default connect(null, mapDispatchToProps)(Login);