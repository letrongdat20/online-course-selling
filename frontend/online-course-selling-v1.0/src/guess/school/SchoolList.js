import React  from 'react';
import { connect } from 'react-redux';
import { SCHOOL } from 'guess/const';
import Topbar from 'base_component/Topbar';
import Footer from 'base_component/Footer';
import { Link } from 'react-router-dom';

function mapStateToProps(state) {
    return {
        ...state.user
    }
}
const SchoolList = (props) => {
    return(
        <div>
            <Topbar isLoggedIn={props.isLoggedIn} />
            <div className="container py-5">
            <h1 className="text-center">Schools and Partners</h1>
            <hr style={{borderWidth: "2px", borderColor: 'black' }}/>
                <ul className="row justify-content-center list-unstyled">
                    {SCHOOL.map((item, index) => {
                        return (
                            <li key={index} className="my-5">
                                <Link role='link' className="text-decoration-none mx-3" to={item.link}>
                                    <img src={item.img} alt="" style={{ maxHeight: '150px', maxWidth: '150px' }} />
                                </Link>
                            </li>
                        );
                    })}
                    {SCHOOL.map((item, index) => {
                        return (
                            <li key={index} className="my-5">
                                <Link role='link' className="text-decoration-none mx-3" to={item.link}>
                                    <img src={item.img} alt="" style={{ maxHeight: '150px', maxWidth: '150px' }} />
                                </Link>
                            </li>
                        );
                    })}
                    {SCHOOL.map((item, index) => {
                        return (
                            <li key={index} className="my-5">
                                <Link role='link' className="text-decoration-none mx-3" to={item.link}>
                                    <img src={item.img} alt="" style={{ maxHeight: '150px', maxWidth: '150px' }} />
                                </Link>
                            </li>
                        );
                    })}
                </ul>
            </div>
            <Footer />
        </div>
    );
}
export default connect(mapStateToProps)(SchoolList);