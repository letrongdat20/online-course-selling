export const USER = {
    userName: "dat.lt",
    password: "12",
    avatar: "/image/avatar/avatar.png"
}

export const COURSE_1 = {
    id: "1",
    title: "Introduction to Reactjs",
    subTitle: "Beginer in reactjs and build a project, we will explore how react work and using",
    decription: "In this course, you will gain an understanding of time-honored financial concepts and rules, and how these can be applied to value firms, bonds, and stocks",
    learn: ["How to value any asset", "Compute return on any project"],
    lesson: ["The time value of money and present value", "Net present value and internal rate of return rule", "Capital budgeting"],
    author: "Me",
    school: "Unknown",
    subject: "Development",
    level: "Introduction",
    language: "English",
    cost: "10",
    link: "/courses/reactjs/enroll",
    star: "4.5",
    img: "/image/courses/cs50-1.jpg"
}
const COURSE_2 = {
    title: "Python base for beginer",
    author: "Me again",
    cost: "30",
    link: "/courses/python/enroll",
    star: "4",
    img: "/image/courses/cs50-1.jpg"
}
const COURSE_3 = {
    title: "Java base for beginner using spring and guilded project",
    author: "You",
    cost: "5",
    link: "/courses/java/enroll",
    star: "3.5",
    img: "/image/courses/cs50-1.jpg"
}
const SCIENCE = [COURSE_1, COURSE_2, COURSE_3, COURSE_2, COURSE_1]
const BUSSINESS = [COURSE_3, COURSE_2, COURSE_1]
const PHOTO = [COURSE_1, COURSE_2, COURSE_3]
const ENGINEER = [COURSE_2, COURSE_3, COURSE_1]
const DESIGN = [COURSE_2, COURSE_1, COURSE_3]
export const COURSES = [SCIENCE, BUSSINESS, PHOTO, ENGINEER, DESIGN]

export const SCHOOL = [
    {
        name: 'HUST',
        address: 'so 1 dai co viet',
        img: './image/school/hust_icon.png',
        link: '/school/hust'
    },
    {
        name: 'NEU',
        address: 'unlnown',
        img: './image/school/neu_icon.png',
        link: '/school/neu'
    },
    {
        name: 'FTU',
        address: 'ha',
        img: './image/school/ftu_icon.png',
        link: '/school/ftu'
    },
    {
        name: 'NUCE',
        address: 'ma',
        img: './image/school/nuce_icon.png',
        link: '/school/nuce'
    },
    {
        name: 'HMU',
        address: 'ma',
        img: './image/school/hmu_icon.png',
        link: '/school/hmu'
    },
    {
        name: 'UET',
        address: 'ma',
        img: './image/school/uet_icon.jpg',
        link: '/school/uet'
    }
]
export const CATEGORIES = [
    {
        link: "/browse/it",
        category: "Information Technology",
        src: "/image/background/it.png"
    },
    {
        link: "/browse/bussiness",
        category: "Bussiness",
        src: "/image/background/business.png"
    },
    {
        link: "/browse/peronal-development",
        category: "Personal Development",
        src: "/image/background/personal_development.png"
    },
    {
        link: "/browse/arts",
        category: "Arts and Humanities",
        src: "/image/background/arts.png"
    },
    {
        link: "/browse/math-and-logic",
        category: "Math and Logic",
        src: "/image/background/math_and_logic.png"
    },
    {
        link: "/browse/physical-and-engineering",
        category: "Physical and Engineering",
        src: "/image/background/engineering.png"
    },
    {
        link: "/browse/health",
        category: "Health",
        src: "/image/background/health.png"
    },
    {
        link: "/browse/social-science",
        category: "Social Sciences",
        src: "/image/background/social_sciences.png"
    }
]