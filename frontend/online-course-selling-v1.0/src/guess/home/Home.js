import React from 'react';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Topbar from 'base_component/Topbar';
import Masthead from 'base_component/Masthead';
import Icon from 'base_component/Icon';
import Footer from 'base_component/Footer';
import { SCHOOL, CATEGORIES } from 'guess/const';
import * as actions from 'guess/search/SearchAction';

const mapStateToProps = (state) => {
    return {
        ...state.search,
        ...state.user
    }
}

const mapDispatchtoProps = (dispatch) => {
    return {
        search: (infor) => dispatch(actions.search(infor))
    }
}
const searchTab = ["computer", "business", "art", "engineering", "design"];
/**
 * class that render home page
 */
function Home(props) {
    const [topic, setTopic] = useState(0); // set index of topic that is showed
    const searchTopic = (tab) => {
        setTopic(tab);
        var infor = {
            type: "subject",
            subject: searchTab[tab],
            level: "",
            school: "",
            language: "",
            available: ""
        }
        props.search(infor);
    }
    return (
        <div id="home">
            <Topbar isLoggedIn={props.isLoggedIn} />
            <Masthead />

            {/* list of school that share course */}
            <div className="container py-5">
                <ul className="row justify-content-center list-unstyled">
                    {SCHOOL.map((item, index) => {
                        return (
                            <li key={index} className="my-2">
                                <Link role='link' className="text-decoration-none mx-3" to={item.link}>
                                    <img src={item.img} alt="" style={{ maxHeight: '100px', maxWidth: '100px' }} />
                                </Link>
                            </li>
                        );
                    })}
                </ul>
                <Link className="d-block text-center text-truncate text-darkblue text-20" to="/school">
                    <u>See more than 50 member university</u>
                </Link>
            </div>

            <hr style={{ borderTop: "1px solid gray", width: '90%' }} />

            {/* Goal */}
            <div id="goal" className="container-fluid">
                <div className="container pb-5">
                    <h1 className="text-center py-5">Achieve your goals with us</h1>
                    <div className="row text-center">
                        <div className="col-6 col-lg-3 mb-2">
                            <div>
                                <img alt="learn" src="/image/icon/Icon-Learn.png" />
                            </div>
                            <br />
                            <h5>Learn the <br />latest skills</h5>
                        </div>
                        <div className="col-6 col-lg-3 mb-2">
                            <div>
                                <img alt="learn" src="/image/icon/Icon-Get-Ready.png" />
                            </div>
                            <br />
                            <h5> Get ready <br /> for a career </h5>
                        </div>
                        <div className="col-6 col-lg-3 mb-2">
                            <div>
                                <img alt="learn" src="/image/icon/Icon-Earn-Degree.png" />
                            </div>
                            <br />
                            <h5> Earn <br /> a degree </h5>
                        </div>
                        <div className="col-6 col-lg-3 mb-2">
                            <div>
                                <img alt="learn" src="/image/icon/Icon-Upskill.png" />
                            </div>
                            <br />
                            <h5>Upskill your <br /> organization</h5>
                        </div>
                    </div>
                </div>
            </div>

            {/* list of course at top */}
            <div id="top_course" className="container-fluit bg-brown">
                <div className="container py-lg-5 py-2 px-0">
                    <h2 className="text-60 pb-lg-4 py-2">Explore top courses</h2>
                    <ul className="list-unstyled row m-0">
                        <li className="ml-3">
                            <button className="btn btn-block btn-toolbar button-hover" aria-expanded={topic === 0 ? "true" : "false"}
                                onClick={() => { searchTopic(0) }}>Computer Science</button>
                        </li>
                        <li className="ml-3">
                            <button className="btn btn-block btn-toolbar button-hover" aria-expanded={topic === 1 ? "true" : "false"}
                                onClick={() => { searchTopic(1) }}>Bussiness</button>
                        </li>
                        <li className="ml-3">
                            <button className="btn btn-block btn-toolbar button-hover" aria-expanded={topic === 2 ? "true" : "false"}
                                onClick={() => { searchTopic(2) }}>Art</button>
                        </li>
                        <li className="ml-3">
                            <button className="btn btn-block btn-toolbar button-hover" aria-expanded={topic === 3 ? "true" : "false"}
                                onClick={() => { searchTopic(3) }}>Engineering</button>
                        </li>
                        <li className="ml-3">
                            <button className="btn btn-block btn-toolbar button-hover" aria-expanded={topic === 4 ? "true" : "false"}
                                onClick={() => { searchTopic(4) }}>Design</button>
                        </li>
                    </ul>
                    <div className="d-flex flex-wrap">
                        {
                            props.courses.map((item, index) => {
                                return (
                                    <Icon key={index} {...item} className="my-3 col-lg-3 col-md-4 col-6" />
                                );
                            })}
                    </div>
                </div>
            </div>

            {/* categories */}
            <div id="topic" className="container-fluid py-md-5 py-2">
                <div className="container">
                    <h1 className="pb-4">Explore topics and skills</h1>
                    <div className="row d-flex">
                        {CATEGORIES.map((item, index) => {
                            return (
                                <Link className="col-4 my-3 image-hover" to={item.link} key={index}>
                                    <div className="bg-dark"><img src={item.src} alt="science" className="image-block" /></div>
                                    <span className="text-white text-25 text-inside">{item.category}</span>
                                </Link>
                            );
                        })}
                    </div>
                </div>
            </div>

            {/* introduction */}
            <div id="about" className="container-fluid py-5 bg-gray">
                <h1 className="text-center pb-0">Learning for anyone, any where</h1>
                <div className="container mb-5">
                    <div className="row flex-row-reverse justify-content-center">
                        <div className="col-10 col-md-8"> <img src="/image/background/anywhere.jpg" alt="" style={{ objectFit: 'cover', width: "100%" }} /> </div>
                        <div className="col-10 col-md-4 d-flex text-md-left flex-column justify-content-center">
                            <h2 className="mx-2 pb-3">Learn from leading universities and companies</h2>
                            <p className="text-20 mx-2">As a global nonprofit, we're relentlessly pursuing
                            our vision of a world where every learner can access education to unlock their
                                potential, without the barriers of cost or location.</p>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-10 col-md-6"> <img src="/image/background/accessible.png" alt="" style={{ objectFit: 'cover', width: "100%" }} /> </div>
                        <div className="col-10 mt-4 col-md-6 d-flex text-md-left flex-column justify-content-center">
                            <h2 className="mx-2 pb-3">Find flexible, affordable options</h2>
                            <p className="text-20 mx-2 pb-3">Choose from many options including free courses and university degrees at a breakthrough price.
                                Learn at your own pace, 100% online.</p>
                        </div>
                    </div>
                </div>
            </div>

            {/* evaluate */}
            <div className="container-md py-5">
                <h2 className="text-center pb-md-4 p-3">More than 70 million people are already learning on Night Owl</h2>
                <div className="d-flex justify-content-center">
                    <img alt="earth" src="/image/background/earth.png"/>
                </div>
                <div className="row justify-content-center text-center">
                    <div className="col-10 col-md-4 p-3">
                        <div className="text-60 text-primary pb-2">87%</div>
                        <h5>of career seekers report benefits like launching a career path or starting a new career</h5>
                    </div>
                    <div className="col-10 col-md-4 p-3">
                        <div className="text-60 text-primary pb-2">92%</div>
                        <h5>of education seekers report benefits like successfully preparing for
                            an exam or committing to an area of study</h5>
                    </div>
                    <div className="col-10 col-md-4 p-3">
                        <div className="text-60 text-primary pb-2">78%</div>
                        <h5>of all learners report more confidence after learning on Night Owl</h5>
                    </div>
                </div>
            </div>

            <Footer/>
        </div>
    );
}

export default connect(mapStateToProps, mapDispatchtoProps)(Home);