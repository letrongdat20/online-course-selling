import axios from 'axios';
import Footer from 'base_component/Footer';
import Topbar from 'base_component/Topbar';
import React, { useState } from 'react';
import { connect } from 'react-redux';

function mapStateToProps(state) {
    return {
        ...state.user
    }
}
function Contact(props) {
    const [email, setEmail] = useState("");
    const [messageRequired, setMessageRequired] = useState("");
    const [emailMessage, setEmailMessage]  = useState("");
    const [message, setMessage] = useState("");
    const [success, setSuccess] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");
    const emailChange = (event) => {
        let value = event.target.value;
        if (!value) {
            setEmailMessage("Email is required.");
        }
        else {
            setEmail(value);
            setEmailMessage("");
            setErrorMessage("");
        }
    }
    const messageChange = (event) => {
        let value = event.target.value;
        if (!value) {
            setMessageRequired("Email is required.");
        }
        else {
            setMessage(value);
            setMessageRequired("");
            setErrorMessage("");
        }
    }
    const send = (event) => {
        event.preventDefault()
        if (email != "" && message != "") {
            axios.post("/api/feedback", {}).then(
                function(response){
                    setSuccess(true);
                }
            ).catch(
                function(error) {
                    setSuccess(true);
                }
            )
        }
        else {
            setErrorMessage("Fill all required field.");
        }
    }
    return (
        <div>
            <Topbar isLoggedIn={props.isLoggedIn} />
            <div className="masthead" style={{ backgroundImage: "url(image/background/contact-bg.jpg)" }}>
                <div className="overlay">
                    <div className="container position-relative px-4 px-lg-5">
                        <div className="row justify-content-center">
                            <div className="col-md-10 col-lg-8 col-xl-7 text-center">
                                <div className="masthead-title mb-4">
                                    <span>Contact Me</span>
                                </div>
                                <span className="masthead-subtitle">Have questions? I have answers.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* Main Content */}
            <main className="mb-4">
                <div className="container px-4 px-lg-5 py-4 py-lg-5">
                    <div className="row justify-content-center">
                        <div className="col-md-10 col-lg-8 col-xl-7">
                            <p className="text-20">Want to get in touch? Fill out the form below to send me a message and I will get back to you as soon as possible!</p>
                            <div className="my-5">
                                {/* * * * * * * * * * * * * * * *
                                * * SB Forms Contact Form * *
                                * * * * * * * * * * * * * * *
                                This form is pre-integrated with SB Forms.
                                To make this form functional, sign up at
                                https://startbootstrap.com/solution/contact-forms
                                to get an API token */}
                                <form id="contactForm">
                                    <div className="form-floating">
                                        <input className="form-control" id="name" type="text" placeholder="Enter your name..." />
                                    </div>
                                    <div className="form-floating">
                                        <input className="form-control" id="email" type="email" placeholder="Enter your email..."
                                            onBlur={emailChange} />
                                        <div className="error">{emailMessage}</div>
                                    </div>
                                    <div className="form-floating">
                                        <input className="form-control" id="phone" type="tel" placeholder="Enter your phone number..." />
                                    </div>
                                    <div className="form-floating">
                                        <textarea className="form-control" id="message" placeholder="Enter your message here..." 
                                            style={{ height: "12rem" }} onBlur={messageChange}></textarea>
                                        <div className="error">{messageRequired}</div>
                                    </div>
                                    <br />
                                    {/* Submit success message
                                    This is what your users will see when the form
                                    has successfully submitted */}
                                    <div>
                                        <div className="text-center mb-3">
                                            <div className="fw-bolder">{success?"Send message successful!":""}</div>
                                        </div>
                                    </div>
                                    {/*Submit error display-none message
                                    This is what your users will see when there is
                                    an error display-none submitting the form */}
                                    <div id="submitError"><div className="text-center text-danger mb-3">{errorMessage != ""?errorMessage:"" }</div></div>
                                    {/* Submit Button */}
                                    <button className="btn btn-primary text-uppercase disabled" id="submitButton" onClick={send}>Send</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <Footer />
        </div>
    );
}
export default connect(mapStateToProps)(Contact);