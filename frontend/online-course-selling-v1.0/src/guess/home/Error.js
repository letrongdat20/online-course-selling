import React from 'react';

function Error(props) {
    return (
        <div>
            <h1>404 We cannot seem to find the page you are looking for</h1>
        </div>
    )
}
export default Error;