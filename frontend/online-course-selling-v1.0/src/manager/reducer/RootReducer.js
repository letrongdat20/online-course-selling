import {combineReducers} from "redux";
import LoginReducer from "manager/reducer/LoginReducer";
import CourseReducer from 'manager/reducer/CourseReducer';
import SearchReducer from 'manager/reducer/SearchReducer';
import LessonReducer from "manager/reducer/LessonReducer";
//return globle state that contain state of all reducer
// every reducer return a state
export default combineReducers({
    user: LoginReducer,
    course: CourseReducer,
    search: SearchReducer,
    lesson: LessonReducer
});