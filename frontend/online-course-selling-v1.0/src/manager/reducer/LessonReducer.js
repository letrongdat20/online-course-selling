import { MODULE } from "student/const";

export default function LessonReducer(state = { module: MODULE }, action) {
    switch (action.type) {
        case "module":
            return {
                ...action.payload
            };

        default:
            return state;
    }
}