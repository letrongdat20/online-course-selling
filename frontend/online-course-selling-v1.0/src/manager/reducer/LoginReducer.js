import { USER } from 'guess/const';

// return user object that has username, password, avatar
export default function LoginReducer(state=USER, action) {
    switch (action.type) {
        case "login":
            return {
                ...action.payload,
                isLoggedIn: true
            };
        default:
            return state;
    }
}