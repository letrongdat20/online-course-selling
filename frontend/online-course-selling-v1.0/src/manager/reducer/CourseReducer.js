import { COURSE_1 } from 'guess/const';

function CourseReducer(state=COURSE_1, action) {
    switch (action.type) {
        case "course":
            return {
                ...action.payload
            }
        case "enroll":
            return {
                ...action.payload
            }
        case "lesson":
            return {
                ...action.payload
            }
        default:
            return state;
    }
}
export default CourseReducer;