import { COURSES } from 'guess/const';

export default function SearchReducer(state = { courses: COURSES[1], volumn: 3000 }, action) {
    switch (action.type) {
        case "courses":
            return {
                ...action.payload
            };
        case "all":
            return {
                ...action.payload
            };
        case "project":
            return {
                ...action.payload
            };
        default:
            return state;
    }
}