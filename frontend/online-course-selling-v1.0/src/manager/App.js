import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import Home from 'guess/home/Home';
import Login from 'guess/login-register/Login';
import Register from 'guess/login-register/Register';
import ForgotPassword from 'guess/login-register/Forgotpass';
import Search from 'guess/search/Search';
import CourseView from 'guess/course-view/CourseView';
import Error from 'guess/home/Error';

import Class from 'student/join-course/Class';
import Lesson from 'student/join-course/Lesson';

import SchoolList from 'guess/school/SchoolList';
import Contact from 'guess/home/Contact';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <Route path="/forgot-password" component={ForgotPassword} />
        <Route path="/search" component={Search} />
        <Route path="/courses/:course_id/class/:lesson_id/learn" component={Lesson} />
        <Route path="/courses/:course_id/class" component={Class} />
        <Route path="/courses/:course_id/enroll" component={CourseView} />
        <Route path="/school" component={SchoolList} />
        <Route path="/contact" component={Contact} />
        <Route path="*" component={Error} />
      </Switch>
    </Router>
  );
}

export default App;
