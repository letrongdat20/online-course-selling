import React from "react";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

const mapStateToProps = state => (
    {
        ...state
    });

// login and logout button that render when guess is not login
function LoginButton(props) {
    return (
        <div className={props.className}>
            <Link className="btn btn-primary m-lg-2 col-lg-auto mb-2 px-4" role='button'
                style={{ fontSize: "20px", fontWeight: 'bold' }} to="/login">Login</Link>
            <Link className="btn btn-danger col-lg-auto" role='button'
                style={{ fontSize: "20px", fontWeight: 'bold' }} to='/register'>Register</Link>
        </div>
    );
}

/**
 * @param - string - props.className css class of element
 * @param - string - props.avatar link to avatar image
 * @returns component that render avatar and dropdown menu in top of page
 */
function Account(props) {
    return (
        <div className={props.className}>
            <a className="text-decoration-none dropdown-toggle" href="#home" id="userDropdown" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img className="rounded-circle" alt="avatar"
                    src={props.avatar} style={{ width: "35px" }} />
            </a>
            {/* Nav Item - User Information */}
            <div className="dropdown no-arrow d-none d-lg-inline">
                {/* Dropdown - User Information */}
                <div className="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                    aria-labelledby="userDropdown">
                    <Link className="dropdown-item" to="/dashboard">
                        <i className="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                            Dashboard
                    </Link>
                    <Link className="dropdown-item" to="/profile">
                        <i className="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                            Profile
                    </Link>
                    <Link className="dropdown-item" to="/account">
                        <i className="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                            Account
                    </Link>
                    <div className="dropdown-divider"></div>
                    <a className="dropdown-item" href="#home" data-toggle="modal" data-target="#logoutModal">
                        <i className="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                        Logout
                    </a>
                </div>
            </div>
        </div>
    );
}

function Topbar(props) {
    return (
        <nav className="navbar navbar-expand-lg navbar-light py-0" id="topBar">
            <div className="container-lg">
                <a className="navbar-brand js-scroll-trigger ml-lg-3 mx-auto" href="/">
                    <img src="/image/logo/owl_logo.png" style={{ width: "60px" }} alt="..." />
                </a>
                <button className="navbar-toggler navbar-toggler-right p-2 mb-3" type="button"
                    data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
                    aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i className="fas fa-bars ml-1"></i>
                </button>
                <div className="collapse navbar-collapse" id="navbarResponsive">
                    <ul className="navbar-nav font-weight-bold text-dark" style={{ fontSize: "20px" }}>
                        {props.isLoggedIn
                            ? <div>
                                <li className="nav-item d-lg-none"><a className="nav-link js-scroll-trigger" href="#dashboard">Dashboard</a></li>
                                <li className="nav-item d-lg-none"><a className="nav-link js-scroll-trigger" href="#profile">Profile</a></li>
                                <li className="nav-item d-lg-none"><a className="nav-link js-scroll-trigger" href="#account">Account</a></li>
                                <li className="nav-item d-lg-none"><a className="nav-link js-scroll-trigger" href="#logout">Logout</a></li>
                            </div>
                            : <LoginButton className="d-lg-none" />
                        }
                        <li className="nav-item"><a className="nav-link js-scroll-trigger" href="/#top_course">Top Course</a></li>
                        <li className="nav-item"><a className="nav-link js-scroll-trigger" href="/#topic">Topic</a></li>
                        <li className="nav-item"><a className="nav-link js-scroll-trigger" href="/#about">About</a></li>
                        <li className="nav-item"><a className="nav-link js-scroll-trigger" href="/school">School</a></li>
                        <li className="nav-item"><a className="nav-link js-scroll-trigger" href="/contact">Contact</a></li>
                    </ul>
                    {props.isLoggedIn
                        ? <Account className="pb-3 ml-auto d-none d-lg-inline" avatar={props.user.avatar} />
                        : <LoginButton className="ml-auto d-none d-lg-inline" />
                    }
                </div>
            </div>
        </nav>
    );
}

export default connect(mapStateToProps, null)(Topbar)