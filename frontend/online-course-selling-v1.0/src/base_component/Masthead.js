import React from 'react';
import { Link } from "react-router-dom";

/**
 * @returns component that render masthead in home page
 */
function Masthead(props) {
    return (
        <div id="#home" className="masthead" style={{ backgroundImage: "url(/image/background/masthead.jpg)" }}>
            <div className="container-lg pb-5">
                <div className="masthead-title p-md-5 p-3 col-10">
                    <span>Restless learners
                    <br />
                        <span style={{ color: "#d23228" }}>change the world</span>
                    </span>
                </div>
                <div className="col-md-10 col-lg-8 col-xl-7 px-md-5 py-4">
                    <form>
                        <div className="form-row">
                            <div className="col-12 col-md-9 mb-2 mb-md-0"><input className="form-control form-control-lg" type="text" placeholder="Tìm khóa học" /></div>
                            <div className="col-12 col-md-3"><button className="btn btn-block btn-lg btn-danger" type="submit">Tìm kiếm</button></div>
                        </div>
                    </form>
                </div>
                <Link to="/search" className="text-white text-25 p-md-5 p-3"><u>Explore all course</u></Link>
            </div>
        </div>
    );
}

export default Masthead