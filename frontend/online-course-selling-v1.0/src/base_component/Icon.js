import React from 'react';
import { Link } from 'react-router-dom';
import TextTruncate from 'react-text-truncate';

/**
 * render title of course in 3 lines
 * @param {object} props: text{string} title of sourse, className{object}: class of element
 * @returns 
 */
function Trancate(props) {
    return (
        <div style={{ lineHeight: '1', height: "83px" }}><TextTruncate containerClassName={props.className}
            text={props.text} line={3} element="h5" />
        </div>);
}
/**
 * function return a card that show information of course
 * @param {string} props.title - title of course
 * @param {string} props.img - link to image of course
 * @param {string} props.author - author of course
 * @param {int} props.star - evaluate of course
 * @param {float} props.price - price of course by dolla
 */
function Icon(props) {
    return (
        <div className={props.className}>
            <Link className="card text-black-90 link-hover" to={props.link}>
                <img className="card-img-top" alt="..." src={props.img} style={{ maxWidth: "270px", maxHeight: "100px", objectFit: "cover" }} />
                <div className="card-body pb-2 pt-3">
                    <Trancate text={props.title} className="card-title" />
                    <div className="card-text">{props.author}</div>
                    <div className="star">{props.star}</div>
                    <div className="card-text">${props.cost}</div>
                </div>
            </Link>
        </div>
    );
}

export default Icon