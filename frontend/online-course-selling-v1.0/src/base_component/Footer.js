import React from "react";
import { Link } from 'react-router-dom';

/**
 * Footter of page
 *  */
function Footer(props) {
    return (
        <footer id="contact" className="container-fluit py-3 bg-green">
            <div className="container">
                <div className="d-flex flex-column px-3">
                    <div className="d-flex justify-content-between mb-4">
                        <Link to="#home"><img className="logo" alt="logo" src="/image/logo/owl_logo.png" /></Link>
                        <ul className="social-link list-unstyled d-flex flex-row">
                            <li className="mx-2">
                                <Link to="http://facebook.com"><i className="fab fa-facebook"
                                    style={{ width: "40px", height: "40px" }}></i></Link>
                            </li>
                            <li className="mx-2">
                                <Link to="http://twitter.com"><i className="fab fa-twitter"
                                    style={{ width: "40px", height: "40px" }}></i></Link>
                            </li>
                            <li className="mx-2">
                                <Link to="http://linkedin.com"><i className="fab fa-linkedin"
                                    style={{ width: "40px", height: "40px" }}></i></Link>
                            </li>
                            <li className="mx-2">
                                <Link to="http://reddit.com"><i className="fab fa-reddit"
                                    style={{ width: "40px", height: "40px" }}></i></Link>
                            </li>
                        </ul>
                    </div>
                    <div className="d-flex flex-wrap justify-content-center justify-content-md-start flex-wrap">
                        <div className="mb-4 mr-5">
                            <h2 className="mb-4 text-25">Night Owl</h2>
                            <ul className="list-unstyled">
                                <li className="mb-2">
                                    <Link className="footter-link" to="/about">About</Link>
                                </li>
                                <li className="mb-2">
                                    <Link className="footter-link" to="/career">Career</Link>
                                </li>
                                <li className="mb-2">
                                    <Link className="footter-link" to="/news">News</Link>
                                </li>
                            </ul>
                        </div>
                        <div className="mr-5">
                            <h2 className="mb-4 text-25">Connect</h2>
                            <ul className="list-unstyled">
                                <li className="mb-2">
                                    <Link className="footter-link" to="/blog">Blog</Link>
                                </li>
                                <li className="mb-2">
                                    <Link className="footter-link" to="/contact">Contact Us</Link>
                                </li>
                                <li className="mb-2">
                                    <Link className="footter-link" to="/help">Help Center</Link>
                                </li>
                                <li className="mb-2">
                                    <Link className="footter-link" to="/donate">Donate</Link>
                                </li>

                            </ul>
                        </div>
                        <div>
                            <Link to="https://iturn.apple.com" className="m-2">
                                <img alt="store" src="/image/logo/apple-store.svg" />
                            </Link>
                            <Link to="https://play.google.com">
                                <img alt="chplay" src="/image/logo/google-play.png" style={{ height: "40px" }} />
                            </Link>
                        </div>
                    </div>
                    <p className="text-center">© 2021 Night Owl Inc. All rights reserved.</p>
                </div>
            </div>
        </footer>);
}
export default Footer;