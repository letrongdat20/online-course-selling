import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Topbar from 'base_component/Topbar';
import Footer from 'base_component/Footer';

const mapStateToProps = (state) => {
    return {
        course: state.course,
        lesson: state.lesson
    }
}

function ModuleCard(props) {
    return (
        <div className="card m-3 bg-green">
            <h5 className="card-header">Module {props.order}: {props.title}</h5>
            <ul className="card-body list-unstyled mb-0">
                {props.lesson.map((item, index) => {
                    return (
                        <li key={index}>
                            {index > 0 ? <hr /> : null}
                            <span className="px-3"><i className="far fa-check-circle" style={{ color: "#dadcdc" }}></i></span>
                            <Link className="card-text text-20" to={props.localLink + "/" + item.id + "/learn"}>{item.title}</Link>
                        </li>
                    );
                })}
            </ul>
        </div>
    );
}

function Class(props) {
    return (
        <div>
            <Topbar isLoggedIn="true" />
            <hr style={{ borderTopWidth: "1px", borderTopColor: "rgba(0, 0, 0, 0.12)" }} className="shadow" />
            <div className="container mb-5">
                <div>
                    <h1 className="mt-4 mb-2">{props.course.title}</h1>
                    <p>by {props.course.author}</p>
                </div>
                <div className="row">
                    <div className="col col-12 col-lg-8">
                        {props.lesson.module.map((item, index) => {
                            return <ModuleCard localLink={props.location.pathname} order={index + 1} title={item.title} lesson={item.lesson} key={index} />
                        })}
                    </div>
                    <div className="col col-12 col-lg-4">
                        <div className="d-flex align-items-center flex-column">
                            <h2>About</h2>
                            <img src="/image/avatar/avatar.png" alt="author" className="rounded-circle"
                                style={{ objectFit: "cover", width: "50%", maxWidth: "200px", aspectRatio: "1/1" }} />
                            <h5 className="d-block">{props.course.author}</h5>
                        </div>
                        <div>
                            <h2 className="text-center mt-5 pb-3">Discussion</h2>
                            <hr />
                            <p>comment 1</p>
                            <hr />
                            <p>comment 2</p>
                            <hr />
                            <p>comment 3</p>
                            <hr />
                            <form>
                                <div className="form-row">
                                    <div className="col-12 col-lg-9 mb-2 mb-md-0"><input className="form-control form-control-lg" type="text" placeholder="Discussion" /></div>
                                    <div className="col-12 col-lg-3"><button className="btn btn-block btn-lg btn-danger" type="submit">Sent</button></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>);
}
export default connect(mapStateToProps, null)(Class);