import React, { useState } from "react";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";
import Topbar from 'base_component/Topbar';

const mapStateToProps = (state) => {
    return {
        lesson: state.lesson
    }
}

function Lesson(props) {
    const [menu, setMenu] = useState(true);
    const [lesson, setLesson] = useState(0);
    const [module, setModule] = useState(0);
    const { lesson_id } = useParams();
    const parseLink = (id) => {
        let path = props.location.pathname;
        path = path.replace(lesson_id, id);
        props.history.push(path);
    }
    return (
        <div>
            <Topbar isLoggedIn="true" />
            <hr className="m-0" />
            <div className="container-fluid d-flex flex-row p-0">
                <div className="d-flex">
                    <div className={menu ? null : "d-none scroll-view"} style={{ width: "240px" }}>
                        {props.lesson.module.map((item, index) => {
                            return (
                                <div key={index}>
                                    <h5 className="my-3 text-center text-dark">{item.title}</h5>
                                    {item.lesson.map((l, i) => {
                                        return (
                                            <div key={i} className={"tab-hover " + (String(lesson) === String(i) && String(module) === String(index) ? "bg-green" : "bg-white")}>
                                                <button onClick={() => parseLink(l.id)} className="text-decoration-none text-15 btn btn-block text-left">
                                                    <i className="fas fa-check-circle mx-3" style={{ color: "green", fontSize: "20px" }}></i>{l.title}</button>
                                            </div>);
                                    })}
                                </div>
                            );
                        })}
                    </div>
                    <div style={{ borderLeft: "1px solid rgba(0, 0, 0, 0.2)" }} className="shadow-right">
                        <button className="btn btn-block" onClick={() => setMenu(!menu)}><i className="fas fa-bars"></i></button>
                    </div>
                </div>
                <div className="scroll-view flex-grow-1">
                    <div style={{ margin: "0% 10%" }}>
                        <h2 className="my-3">{props.lesson.module[module].lesson[lesson].title}</h2>
                        <div className="video-course">
                            <video controls style={{ width: "100%" }}>
                                <source src="/video/index.mp4" type="video/mp4" />
                                <source src="/video/index.ogg" type="video/ogg" />
                                Your browser does not support the video
                            </video>
                        </div>
                        <form className="d-flex justify-content-between mt-2">
                            <button className="btn btn-default btn-primary btn-lg text-20">Previous</button>
                            <button className="btn btn-default btn-primary btn-lg px-4 text-20">Next</button>
                        </form>
                        <hr />
                        <div>
                            translate essay
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default connect(mapStateToProps, null)(Lesson);